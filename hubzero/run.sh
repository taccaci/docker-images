#!/bin/bash

# Start mysql
/etc/init.d/mysql start

# Start apache
/usr/sbin/apache2ctl start
/usr/bin/tail -f /var/log/apache2/error.log
