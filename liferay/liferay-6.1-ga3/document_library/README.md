# Liferay Document Library

If you want to preserve your Liferay Document Library, you can mount it as a volume when you run the docker image.

```bash
docker run idP -v $(pwd)/document_library:/liferay/document_library taccaci/liferay-6.1-ga3
```
