# Liferay 6.1-GA3 Community Edition

Docker image for Liferay 6.1-GA3 CE.

## Configuration

You can build Tomcat and Liferay configuration into the container
for `portal-ext.properties`, `context.xml`, and `server.xml`.

### Portal properties

You can override portal properties by modifying `portal-ext.properties`.

### Tomcat properties

Edit `context.xml` to add any Context config as necessary.

Edit `server.xml` to customize the Tomcat server config.

Add any keys to `keystore`. This will be used by Tomcat for
SSL as well as any keys that need to be imported for SSL validation.
The included keystore file here includes a self-signed tomcat SSL
certificate as well as the InCommon CA chain.

## Including Liferay Plugins

You can deploy Liferay plugins to this container by mounting a volume to `/liferay/deploy`. The `deploy/` directory here is provided as an example.

## Document Library

You can also preserve a Document Library across containers by mounting a volume to `/liferay/document_library`. The `document_library/` directory here is provided as an example.

## Database

This container does not include a database, but Liferay will create and use an internal Hypersonic database by default. This isn't appropriate for production use. You can configure the Liferay instance inside the container to connect to an external database if that is necessary.

## Running the container

The following command is a complete example of running this container with all necessary parameters:

```bash
$ docker run \
    -e "XCDB_USER=<username>" \
    -e "XCDB_PASSWORD=<password>" \
    -e "XCDB_HOST=<example.com>" \
    -e "XCDB_PORT=5432" \
    -e "XCDB_DATABASE=database" \
    -v $(pwd)/deploy:/liferay/deploy \
    -v $(pwd)/document_library:/liferay/document_library \
    -dP taccaci/liferay-6.1-ga3
```
