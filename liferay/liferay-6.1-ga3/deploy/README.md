# Deploy apps

Puts WARs in here to be deployed to the Liferay application.


Mount this directory when you run the docker container:

```bash
docker run -dP -v $(pwd)/deploy:/liferay/deploy taccaci/liferay-6.1-ga3
```
