```
#!bash
    ___   __________   ____             __            
   /   | / ____/  _/  / __ \____  _____/ /_____  _____
  / /| |/ /    / /   / / / / __ \/ ___/ //_/ _ \/ ___/
 / ___ / /____/ /   / /_/ / /_/ / /__/ ,< /  __/ /    
/_/  |_\____/___/  /_____/\____/\___/_/|_|\___/_/     
```

-----

- Read and improve the [Wiki](https://bitbucket.org/taccaci/docker-images/wiki)

- Check the [open issues](https://bitbucket.org/taccaci/docker-images/issues?status=new&status=open).

## Installing Docker
===================

### Centos:
	
	sudo yum install docker-io
	sudo chkconfig --add docker
	sudo chkconfig --level 2345 docker on
	sudo service docker start
	
### Ubuntu:

	curl -sSL https://get.docker.io/ubuntu/ | sudo sh

### Mac

The easy way if you're not using volumes natively

	brew install docker boot2docker

That’s formally enough to run containers. But boot2docker doesn’t work with volumes, which is an important topic. Follow these steps to have an improved image which allows you to mount volumes in the containers:

	$ boot2docker init
	$ boot2docker stop
	$ curl -LO http://static.dockerfiles.io/boot2docker-v1.2.0-virtualbox-guest-additions-v4.3.14.iso
	$ mv boot2docker-v1.2.0-virtualbox-guest-additions-v4.3.14.iso ~/.boot2docker/boot2docker.iso
	$ VBoxManage sharedfolder add boot2docker-vm -name home -hostpath /Users
	$ boot2docker start

### Windows

You're better than that.


## Building images
===================
Each container directory contains a separate Dockerfile which will build the desired container. The agave-test-base container's Dockerfile is in the root folder and must be build and tagged first prior to building any other containers.

	$ docker build -rm -t agave-test-base .

## Orchestrating Infrastructure
===================

The Docker PaaS field is very active right now. We've found that the easiest solutions right now are 
* [Fig](http://fig.sh) for single node deployments
* [Shipyard](http://shipyard-project.com/) for multi-node clusters
* [Kubernetes](https://github.com/GoogleCloudPlatform/kubernetes) for managed, scalable, multi-tier application deployments.

## Misc
===================

* Volume mounting on boot2docker will probably cause you issues. Try using v1.1.0 or greater.
* The following script will startup boot2docker, configure the vagrant/vb ports, and set your host variable properly.

```
#!bash

#!/bin/bash
# dockerup
# 
# Script to startup docker using boot2docker and forward the normal docker
# port ranges from host to vm so we can interact with the containers as if
# they were running locally.

# vm must be powered off
for i in {49000..49900}; do
 VBoxManage modifyvm "boot2docker-vm" --natpf1 "tcp-port$i,tcp,,$i,,$i";
 VBoxManage modifyvm "boot2docker-vm" --natpf1 "udp-port$i,udp,,$i,,$i";
done

# startup boot2docker
boot2docker up

# setup the env...hoping this is a constant value
export DOCKER_HOST=tcp://$(boot2docker ip 2>/dev/null):2375
```


