#!/bin/bash

if [[ -z "$flavor" ]]; then

    FLAVORS=($(nova flavor-list | grep '^| ' | grep '\.' | awk '{print $4}'))

    echo "Please specify an instance size from the following list:"
    for i in "${!FLAVORS[@]}"
    do
      echo "[$i] ${FLAVORS[$i]}"
    done

    read -e -i "${FLAVORS[0]}" -p "Your choice: " flavor
fi

if [[ -z "$security_group" ]]; then

    # SECURITY_GROUPS=$(openstack image list | grep '^| ' | grep '-' | awk '{print $4}')
    #
    # echo "Please specify one or more of the following security groups in a comma-separated list:"
    # for i in "${!SECURITY_GROUPS[@]}"
    # do
    #   echo "[$i] ${SECURITY_GROUPS[$i]}"
    # done
    #
    # read -e -i "${SECURITY_GROUPS[0]}" -p "Your choice [${SECURITY_GROUPS[0]}]: " security_group
    security_group="default"
fi

if [[ -z "$keypair_name" ]]; then

    KEYPAIRS=($(openstack keypair list | grep '^| ' | grep -iv '^| Name' | awk '{print $2}'))

    echo -e "\nPlease specify a login keypair from the following list:"
    for i in "${!KEYPAIRS[@]}"
    do
      echo "[$i] ${KEYPAIRS[$i]}"
    done

    read -e -i "${KEYPAIRS[0]}" -p "Your choice: " keypair_name
fi


if [[ -z "$network" ]]; then

  NETWORKS_RESPONSE=$(nova network-list | grep '^| ' | grep -iv '^| ID')
  NETWORK_NAMES=($(echo "$NETWORKS_RESPONSE" | awk '{print $4}'))

  echo -e "\nPlease specify a network from the following list:"
  for i in "${!NETWORK_NAMES[@]}"
  do
    echo "[$i] ${NETWORK_NAMES[$i]}"
  done

  read -e -i "${NETWORK_NAMES[0]}" -p "Your choice: " network_name

  network_uuid=$(echo "$NETWORKS_RESPONSE" | grep "$network_name" | awk '{print $2}')

fi

if [[ -z "$image" ]]; then

    IMAGES=($(openstack image list 2>/dev/null | grep '^| ' | grep -iv '| Name ' | awk '{print $4}'))

    echo -e "\nPlease specify an image from the following list:"
    for i in "${!IMAGES[@]}"
    do
      echo "[$i] ${IMAGES[$i]}"
    done

    read -e -i "${IMAGES[0]}" -p "Your choice: " image
fi

if [[ -z "$vm_name" ]]; then
  UUID=$(cat /proc/sys/kernel/random/uuid)
  DEFAULT_VM_NAME="$OS_USERNAME-$UUID"

  echo ""
  read -e -i "$DEFAULT_VM_NAME" -p "Please specify a name for your instance: " vm_name
fi

echo -e "\nCreating new instance..."

#echo -e "\nnova boot --flavor $flavor --image $image --nic net-id=$network_uuid --security-group $security_group --key-name $keypair_name $vm_name"
instance_response=$(nova boot --flavor "$flavor" --image "$image" --nic net-id="$network_uuid" --security-group "$security_group" --key-name "$keypair_name" $vm_name)
echo "$instance_response"

instance_id=$(echo "$instance_response" | grep -i "^| id " | awk '{print $4}')

if [[ -z "$instance_id" ]]; then
  echo -e "\nFailed to create new instance."
  exit 1
fi

echo -e "\nSuccessfully created new instace $instance_id\n"

read -e -i "Y" -p "Would you like to add a floating ip address [Y]/n: " add_floating_ip

if [[ "Y" == "$add_floating_ip" ]]; then

  FLOATING_IP_RESPONSE=$(openstack ip floating list | grep '^| ' | grep -iv '^| ID' | grep -i '| None ')

  if [[ "${!FLOATING_IP_RESPONSE[@]}" -eq "0" ]]; then

    FLOATING_IPS=($(echo "$FLOATING_IP_RESPONSE" | awk '{print $6}'))

    echo -e "\nPlease specify a floating ip to assign from the following list:"
    for i in "${!FLOATING_IPS[@]}"
    do
      echo "[$i] ${FLOATING_IPS[$i]}"
    done

    read -e -i "${FLOATING_IPS[0]}" -p "Your choice: " floating_ip

  else

    FLOATING_IP_POOLS=($(openstack ip floating pool list | grep '^| ' | grep -iv '^| Name' | awk '{print $2}'))

    echo -e "\nPlease specify the pool from which to obtain the floating ip:"
    for i in "${!FLOATING_IP_POOLS[@]}"
    do
      echo "[$i] ${FLOATING_IP_POOLS[$i]}"
    done

    read -e -i "${FLOATING_IP_POOLS[0]}" -p "Your choice: " ip_pool

    floating_ip=$(openstack ip floating create "$ip_pool" | grep '^| ' | grep -i '^| ip ' | awk '{print $4}')

    echo -e "\nCreated new floating ip $floating_ip in pool $ip_pool"
  fi

  echo "Assigning floating ip $floating_ip to instance $instance_id..."

  ip_assignment_response=$(openstack ip floating add $floating_ip $instance_id)

  echo "Finished assigning floating ip address"

fi

echo "That's all folks!"
