# OpenStack Command Line Client Tools

This is a minimal container hosting the OpenStack CLI. It starts a shell and checks your environment for configuration and authentication credentials, prompting you if they are not found.

## Configuring with `PROJECT`-openrc.sh file

The standard way to initialize your OpenStack environment is through a configuration file. Generally, your configuration file will be named *PROJECT-opensrc.sh* where "PROJECT" is the id of your OpenStack tenant. You can use an existing OpenStack configuration file on your local system to initialize your container's CLI by setting the `OPENRC_LOCATION` environment variable to the path of a `PROJECT-openrc.sh` file within a mounted volume.

For example, if you had a OpenStack configuration file saved on your local system at */home/username/.openstack/FOO-PROJECT-openrc.sh*, then you could auto-initialize your container with the following run command:

	docker run -it	-e OPENRC_LOCATION=/etc/openstack/FOO-PROJECT-openrc.sh \
                  	-v /home/username/.openstack:/etc/openstack             \
                  	tacc/openstack-cli  

If your configuration file is missing any variables, you will be prompted for them.

## Configuration with environment variables

The standard OpenStack environment variables are supported:

  * OS_USERNAME: Your openstack username
  * OS_PASSWORD: Your openstack password
  * OS_TENANT_ID: The id of your tenant
  * OS_TENANT_NAME: The name of your tenant
  * OS_AUTH_URL: The default openstack authentication url. (ex. https://example.com:5000/v2.0)

You can provide these as environment variables at runtime.

	docker run -it	-e OS_USERNAME=username       \
                  	-e OS_PASSWORD=password       \
                  	-e OS_TENANT_ID=tenantId      \
                  	-e OS_TENANT_NAME=tenantName  \
                  	-e OS_AUTH_URL="https://example.com:5000/v2.0" \
                  	tacc/openstack-cli

The container will prompt you for any variables you do not supply in the environment. It is generally good practice to omit your password to avoid it being written to your local history.

## Creating new VM Instances

This image contains an interactive script `create_vm.sh` to start up new VM instances using nova. It will prompt you for the name, flavor, image, network, keypair, security group of your instance. It will optionally also walk you through assigning a public floating ip if you so choose.

	docker run -it -e OPENRC_LOCATION=/etc/openstack/FOO-PROJECT-openrc.sh \
                  	-v /home/username/.openstack:/etc/openstack             \
                  	tacc/openstack-cli create_vm.sh


