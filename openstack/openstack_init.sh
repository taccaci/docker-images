#!/bin/bash

# With the addition of Keystone, to use an openstack cloud you should
# authenticate against keystone, which returns a **Token** and **Service
# Catalog**.  The catalog contains the endpoint for all services the
# user/tenant has access to - including nova, glance, keystone, swift.
#
# *NOTE*: Using the 2.0 *auth api* does not mean that compute api is 2.0.  We
# will use the 1.1 *compute api*
if [[ -n "$OPENRC_LOCATION" ]]; then

  if [[ -e "$OPENRC_LOCATION" ]]; then
    source $OPENRC_LOCATION
  else
    echo "No openrc.sh file found at $OPENRC_LOCATION"
    exit 1
  fi

fi

if [[ -z "$OS_AUTH_URL" ]]; then
  read -r -p "Please enter your OpenStack Auth URL (ex https://example.com:5000/v2.0): " -r OS_AUTH_URL_INPUT
  export OS_AUTH_URL=$OS_AUTH_URL_INPUT
fi

# With the addition of Keystone we have standardized on the term **tenant**
# as the entity that owns the resources.
if [[ -z "$OS_TENANT_ID" ]]; then
  read -r -p "Please enter your OpenStack Tenant ID: " -r OS_TENANT_ID_INPUT
  export OS_TENANT_ID=$OS_TENANT_ID_INPUT
fi

if [[ -z "$OS_TENANT_NAME" ]]; then
  read -r -p "Please enter your OpenStack Tenant Name: " -r OS_TENANT_NAME_INPUT
  export OS_TENANT_NAME=$OS_TENANT_NAME_INPUT
fi

# In addition to the owning entity (tenant), openstack stores the entity
# performing the action as the **user**.
if [[ -z "$OS_USERNAME" ]]; then
  read -r -p "Please enter your OpenStack Username: " -r OS_USERNAME_INPUT
  export OS_USERNAME=$OS_USERNAME_INPUT
fi

# With Keystone you pass the keystone password.
if [[ -z "$OS_PASSWORD" ]]; then
  read -r -p echo "Please enter your OpenStack Password: " -sr OS_PASSWORD_INPUT
  export OS_PASSWORD=$OS_PASSWORD_INPUT
fi
