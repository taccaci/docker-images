# Dockerfiles to create containers used in development and testing.
================================

## Container users

  root:root
  testuser:testuser
  testshareuser:testshareuser
  testotheruser:testotheruser

## Building Containers

Each container directory contains a separate Dockerfile which will build the desired container. The agave-test-base container's Dockerfile is in the root folder and must be build and tagged first prior to building any other containers.

  $ docker build -rm -t agave-test-base .

## Running Containers

  $ docker run -h docker.example.com -i \
    -p 10022:22     \ # SSHD, SFTP
    -p 59153:49153  \ # Supervisord
    [-p 12811:2811] \ # GridFTP
    [-p 12222:2222] \ # GSISSH
    [-p 17512:7512] \ # MyProxy
    [-p 12222:2222] \ # GSISSH
    [-p 11247:1247] [-e "PAM=true"] [-e "X509=true"] \ # IRODS
    [-p 10021:21]   \ # FTP
    [-p 10443:443]  \ # HTTPS
    [-p 10080:80]   \ # HTTP
    <container name>

## Orchestrating Infrastructure

We recommend [Fig](http://fig.sh) for single node deployments and [Kubernetes](https://github.com/GoogleCloudPlatform/kubernetes) for multi-node deployments.

## Misc

* Volume mounting on boot2docker will probably cause you issues. Try using v1.1.0 or greater.
* The following script will startup boot2docker, configure the vagrant/vb ports, and set your host variable properly.

```
#!/bin/bash
# dockerup
# 
# Script to startup docker using boot2docker and forward the normal docker
# port ranges from host to vm so we can interact with the containers as if
# they were running locally.

# vm must be powered off
for i in {49000..49900}; do
 VBoxManage modifyvm "boot2docker-vm" --natpf1 "tcp-port$i,tcp,,$i,,$i";
 VBoxManage modifyvm "boot2docker-vm" --natpf1 "udp-port$i,udp,,$i,,$i";
done

# startup boot2docker
boot2docker up

# setup the env...hoping this is a constant value
export DOCKER_HOST=tcp://$(boot2docker ip 2>/dev/null):2375
```


